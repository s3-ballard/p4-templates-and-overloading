//
//  array.hpp
//  CommandLineTool
//
//  Created by Scott Ballard on 06/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef array_hpp
#define array_hpp

#include <stdio.h>

template <class Type>
class Array
{
    
public:
    /** constructer*/
    Array()
    {
        numElements = 0;
        TypePoint = nullptr;
    }
    
    /** destructor */
    ~Array()
    {
        delete[] TypePoint;
    }
    
    bool operator==(Array& other)
    {
        if (this->size() != other.size())
        {
            return false;
        }
        for (int i =0; i< size(); i++)
        {
            if (this[i] != other[i])
            {
                return false;
            }
        }
        return true;
    }
    
    /** Adds new elements into the array */
    void add (Type itemValue)
    {
        //create a new pointer array that is one larger then the last item in the list
        Type* temp = new Type[size()+1];
        
        //loop through items in the temporary array and move pointer through them
        for (int counter = 0; counter < size(); counter++)
            temp[counter] = TypePoint[counter];
        
        //the last element in the temporary array is the entered item
        temp[size()] = itemValue;
        
        //if float point does not equal anything then delete the memory array
        if (TypePoint != nullptr)
            //deletes already exisiting values
            delete[] TypePoint;
        
        //pointer stores all the elements of the temporary array;
        TypePoint = temp;
        
        //increase number of elements.
        numElements++;
    }
    
    /** returns item at index */
    Type get (int index) const
    {
        return TypePoint[index];
    }
    
    /** returns number of items in an array */
    int size()
    {
        return numElements;
    }
    
private:
    int numElements;
    Type *TypePoint;
    
    
};

#endif /* array_hpp */

